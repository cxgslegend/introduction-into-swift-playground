//: Playground - noun: a place where people can play

import UIKit

/*****************************************************
    01) Variables
*****************************************************/

var str = "Hello, playground"
str = "Hello, world!"

// Do semicolons matter?
str = "Nope, semicolons don't matter 😎. But its considered bad practice to use them in Swift.";

/*****************************************************
    02) Constants
*****************************************************/

// This is called an immutable type (meaning the value cannot change).
let favoriteProgrammingLanguage = "Swift"
//favoriteProgrammingLanguage = "Java"

/*****************************************************
    03) Naming Conventions
*****************************************************/

// Use camelCase
var namingThingsIsFun = "Yay"

// Don't use emoji's as varible names 
var 😫 = "            ☀️ " +
         "               " +
         "               " +
         "  🏃💨     🚓   ";

/*****************************************************
    04) Concatenation vs Interpolation
*****************************************************/

let firstName = "Craig"
let lastName = "Lovell"

var fullName : String;

// Concatenation method (Apple doesn't like this one).
fullName = firstName + " " + lastName;

// Interpolation method
fullName = "\(firstName) \(lastName)";

//let failedCancatenatedString = 360 + " no scope"
let successfulInterpolatedString = "\(360) no scope"

/*****************************************************
    05) Is Swift a Dynamically Typed or Statically Typed Language?
*****************************************************/

let staticallyTypedStringVariable = ":)"
// staticallyTypedStringVariable = 5 // Wont work becuase the type is actually a string.

// The compiler infers the type of varibles if the type is not given, however we can still make our types explicit if we want to.
let explicitString : String = ":-)"
let explicitInt : Int = 42 // The number of the universe 
let explicitDouble : Double = 3.14159265359
let explicitBoolean : Bool = true

/*****************************************************
    06) Mutability of Value Types vs Mutability of Reference types
*****************************************************/

var mutableArray = ["Something"]
// Or
// var mutableArray : [String] = ["Something]
// Or
// var mutableArray : Array<String> = ["Something"]


mutableArray += ["Something Else"]
// Or
// mutableArray = mutableArray + ["Something Else"]

let list : [String] = ["Bread", "Milk", "Pop",]
//list += ["Eggs"] // Will not work because let made the whole list immutable (not just the reference). This is because an Array is actually a Struct which is a value type. If it was a reference type, then only the reference would be made immutable.

/*****************************************************
    07) Optionals
*****************************************************/

// let constantsAndVariablesCantBeNil : String = nil

// Ok so they can't be nil, but what if you are not sure if you will have an object (think the times that you use null in Java). Well thats when you have to use nil.

let somethingThatCanBeNil : String? = nil

if let temp = somethingThatCanBeNil {
    print("I am not nil")
}
else {
    print("I am nil")
}

// There is some more stuff to know about optionals (like optional chaining, and optional binding). Also there are some really important  concepts that I didn't cover becuase of time (like functions (ie... closures), range operators, tuples, Classes, Structs, Enumerations, Control flow, and delegate patterns)



















